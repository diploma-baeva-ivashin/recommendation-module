exports.index = function(req, res){
    res.render('index', { title: 'Express' });
};

let itemBased = require('../services/itemBased');
let naiveBaies = require('../services/naiveBaies');
let coldStart = require('../services/coldStart');
let popular = require('../services/popular');

function findCourses(userCourses, userId) {
    let thisUserArray = userCourses.filter(function (user) {
        return user.userId === userId;
    });
    return thisUserArray[0];
}

function findKeywords(users, userId) {
    let thisUser = users.filter(function (user) {
        return user.id === userId;
    });
    return thisUser[0].keywords;
}

exports.data = function(req, res){
    let users = req.body.users;
    let courses = req.body.courses;
    let userCourses = req.body.userCourses;
    let keywords = req.body.keywords;
    let userId = req.body.userId;

    let recommend = {};
    let thisUserCourses = findCourses(userCourses, userId);
    let thisUserKeywords = findKeywords(users, userId);

    //Find count of ratings
    let ratingCount = 0;
    thisUserCourses.courses.forEach(function (course, i) {
        course.rating !== null ? ratingCount += 1 : false;
    });


    if (thisUserCourses.courses.length > 0) {
        if (ratingCount >= 3) {
            recommend = itemBased(userId, courses, userCourses);
            console.log('item');
        } else {
            recommend = naiveBaies(userId, users, courses, userCourses, keywords);
            console.log('naiveBaies');
        }
    } else if (thisUserKeywords.length > 0) {
        console.log('coldStart');
        recommend = coldStart(userId, users, userCourses);
    } else {
        recommend = popular(courses);
        console.log('popular');
    }
    console.log(recommend);
    res.end(JSON.stringify(recommend));
};