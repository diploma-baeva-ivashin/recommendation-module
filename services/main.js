let content_based = require('./content_based');
let userBased = require('./userBased');
let itemBased = require('./itemBased');
let naiveBaies = require('./naiveBaies');
let coldStart = require('./coldStart');

// Get info
let request = require('request');
request.get({url: 'http://localhost:3420/api/all'}, function (err, response, body) {
    let info = JSON.parse(body);

    let users = info.users;
    let courses = info.courses;
    let userCourses = info.userCourses;
    let keywords = info.keywords;

// Find courses for user
    let recommend = {};
    let userId = 7;
    let thisUserArray = userCourses.filter(function (user) {
        return user.userId === userId;
    });
    let thisUserCourses = thisUserArray[0];
    let thisUser = users.filter(function (user) {
        return user.id === userId;
    });
    let thisUserKeywords = thisUser[0].keywords;

    //Find count of ratings
    let ratingCount = 0;
    thisUserCourses.courses.forEach(function (course, i) {
        course.rating !== null ? ratingCount += 1 : false;
    });

    if (thisUserCourses.courses.length > 0) {
        if (ratingCount >= 4) {
            // recommend = itemBased(userId, courses, userCourses);
            recommend = naiveBaies(userId, users, courses, userCourses, keywords);
        } else {
            recommend = naiveBaies(userId, users, courses, userCourses, keywords);
        }
    } else if (thisUserKeywords.length > 0) {
        recommend = coldStart(userId, users, userCourses);
    } else {
        recommend = {popular: 'Полулярные курсы'};
    }
    console.log(recommend)
});
