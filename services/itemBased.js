function makeMatrix(userCourses) {
    let matrix = {};

    for (let i = 0; i < userCourses.length; i++) {
        let userId = userCourses[i].userId;
        matrix[userId] = {};
        for (let j = 0; j < userCourses[i].courses.length; j++) {
            let courseId = userCourses[i].courses[j].courseId;
            if (userCourses[i].courses[j].rating !== null) {
                matrix[userId][courseId] = userCourses[i].courses[j].rating;
            }
        }
    }
    return matrix
}

function findAv(matrix) {
    let statistics = {};
    let userAv = {};
    for (let i in matrix) {
        statistics[i] = {sum: 0, count: 0};
        for (let j in matrix[i]) {
            statistics[i].sum += matrix[i][j];
            statistics[i].count += 1;
        }
        userAv[i] = statistics[i].sum / statistics[i].count;
    }
    return userAv
}

function findCorrelation(courseId, matrix) {
    let w = {};
    let sum1 = {};
    let sum2 = {};
    let sum3 = {};
    for (i in matrix) {
        let userId = i;
        if (matrix[userId] !== undefined) {
            let userAv = findAv(matrix)[userId];

            //Find rating for selected course
            let currentCourseRating = matrix[userId][courseId];

            for (let j in matrix[i]) {
                if (j != courseId && currentCourseRating !== undefined && matrix[i][j] !== undefined) {
                    if (sum1[j] === undefined) {
                        sum1[j] = 0;
                        sum2[j] = 0;
                        sum3[j] = 0;
                    }
                    sum1[j] += (currentCourseRating - userAv) * (matrix[i][j] - userAv);
                    sum2[j] += Math.pow((currentCourseRating - userAv), 2);
                    sum3[j] += Math.pow((matrix[i][j] - userAv), 2);
                    w[j] = sum1[j] / (Math.sqrt(sum2[j]) * Math.sqrt(sum3[j]));
                }
            }
        }
    }

    //Sorting w by correlation
    let wArray = [];
    let neighborNum = 10;
    for (let i in w) {
        let coeff = {courseId: i, coeff: w[i]};
        wArray = [...wArray, coeff];
    }

    wArray.sort(function (a, b) {
        if (Math.abs(a.w) < Math.abs(b.w)) {
            return 1;
        }
        if (Math.abs(a.w) > Math.abs(b.w)) {
            return -1;
        }
        return 0;
    });

    return wArray.slice(0, neighborNum);
}

function findRating(userId, courseId, matrix, w) {
    let thisUserCourses = matrix[userId];
    let userAv = findAv(matrix)[userId];
    let recommend = 0;
    let sum1 = 0;
    let sum2 = 0;

    for (let i = 0; i < w.length; i++) {
        let rating = thisUserCourses[w[i].courseId];
        if (rating !== undefined && w[i].courseId !== courseId) {
            sum1 += (rating - userAv) * w[i].coeff;
            sum2 += Math.abs(w[i].coeff);
        }
        recommend = userAv + sum1 / sum2;
    }

    if (recommend > userAv) {
        return recommend
    }
}

function itemBased(userId, courses, userCourses) {
    //Make userCourses table
    let matrix = makeMatrix(userCourses);
    //Find correlation
    let w = {};
    for (let i = 0; i < courses.length; i++) {
        let courseId = courses[i].id;
        w[courseId] = findCorrelation(courseId, matrix);
    }

    //Find rating
    let rating = {};
    for (let i = 0; i < courses.length; i++) {
        let courseId = courses[i].id;
        rating[courseId] = findRating(userId, courseId, matrix, w[courseId]);
        rating[courseId] === undefined ? delete rating[courseId] : false;
    }

    //Check if user pass this course yet
    for (let i in matrix[userId]) {
        rating[i] ? delete rating[i] : false
    }

    //Make an array
    let recommend = [];
    for (i in rating) {
        let course = {courseId: parseInt(i), rating: rating[i]};
        recommend = [...recommend, course];
    }

    return recommend.splice(0, 4);
}

module.exports = itemBased;
