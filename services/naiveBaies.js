function makeClasses(thisUserCourses, courses, userAv, users, userId, allKeywords) {
    let classes = {
        like: {courses: [], count: 0, keywords: {}},
        dislike: {courses: [], count: 0, keywords: {}}
    };
    let className = '';

    //Add user keywords to like
    users.filter(function (user) {
        if (user.id === userId && user.keywords.length > 0) {
            for (key in allKeywords) {
                classes.dislike.keywords[key] = 1;
            }
            user.keywords.forEach(function (word, j) {
                if (classes.like.keywords[word.id] !== undefined) {
                    classes.like.keywords[word.id] += 1;
                    classes.dislike.keywords[word.id] -= 1;
                } else {
                    classes.like.keywords[word.id] = 1;
                }
            });
            classes.like.count += 1;
            classes.dislike.count += 1;
        }
    });

    //Find like and dislike courses
    thisUserCourses.forEach(function (course, i) {
        (course.rating >= userAv && course.progress > 0.5) ? className = 'like' : className = 'dislike';
        classes[className].count += 1;
        classes[className].courses = [...classes[className].courses, course];
    });

    //Find keywords for courses
    for (key in classes) {
        classes[key].courses.forEach(function (userCourse, i) {
            courses.forEach(function (course, j) {
                let id = userCourse.courseId;
                if (course.id === id) {
                    course.keywords.forEach(function (keyword, g) {
                        let words = classes[key].keywords;
                        words[keyword.id] !== undefined ? words[keyword.id] += 1 : words[keyword.id] = 1;
                    })
                }
            })
        });
        delete classes[key].courses;
    }

    return classes;
}

function baies(keywords, classes, course) {
    let commonClassesNum = 0;
    let wordsClassesNum = {like: 0, dislike: 0};
    let wordsNum = 0;
    for (key in keywords) {
        wordsNum += 1;
    }

    for (key in classes) {
        commonClassesNum += classes[key].count;
        for (word in classes[key].keywords) {
            wordsClassesNum[key] !== undefined ? wordsClassesNum[key] += 1 : wordsClassesNum[key] = 1;
        }
    }

    let classesNum = {
        like: classes.like.count / commonClassesNum,
        dislike: classes.dislike.count / commonClassesNum
    };

    let courseKeywords = course.keywords;
    let coeff = {};

    for (key in classes) {
        coeff[key] = Math.log(classesNum[key]);
        courseKeywords.forEach(function (word, i) {
            let w = classes[key].keywords[word.id];
            w === undefined ? w = 1 : w += 1;
            coeff[key] += Math.log((w) / (wordsNum + wordsClassesNum[key]));
        });
    }

    let a = Math.exp(coeff.like) / ( Math.exp(coeff.like) + Math.exp(coeff.dislike));
    let b = Math.exp(coeff.dislike) / ( Math.exp(coeff.like) + Math.exp(coeff.dislike));

    if (a > b) {
        return a
    }
}

function naiveBaies(userId, users, courses, userCourses, allKeywords) {
    let rating = {};
    let userSum = 0;
    let userCount = 0;

    //Find courses of our user
    let thisCourses = userCourses.filter(function (user) {
        if (user.userId === userId) {
            user.courses.forEach(function (course, i) {
                userSum += course.rating;
                userCount += 1;
            })
        }
        return user.userId === userId;
    });
    let thisUserCourses = thisCourses[0].courses;
    let userAv = userSum / userCount;

    //Make classes like and dislike
    let classes = makeClasses(thisUserCourses, courses, userAv, users, userId, allKeywords);
    for (let i = 0; i < courses.length; i++) {
        let courseId = courses[i].id;
        rating[courseId] = baies(allKeywords, classes, courses[i]);
        rating[courseId] === undefined ? delete rating[courseId] : false;
    }

    // Check if user pass this course yet
    for (let i = 0; i < thisUserCourses.length; i++) {
        let courseId = thisUserCourses[i].courseId;
        rating[courseId] ? delete rating[courseId] : false
    }

    //Make an array
    let recommend = [];
    for (i in rating) {
        let course = {courseId: parseInt(i), rating: rating[i]};
        recommend = [...recommend, course];
    }

    return recommend.splice(0, 4);
}

module.exports = naiveBaies;