function similarUsers(userId, users) {
    let userKey = users.filter(function (user) {
        return user.id === userId;
    });
    let userKeywords = userKey[0].keywords;

    let w = [];
    for (let i = 0; i < users.length; i++) {
        if (users[i].id !== userId) {
            w[i] = {userId: users[i].id, count: 0};
            for (j = 0; j < users[i].keywords.length; j++) {
                for (g = 0; g < userKeywords.length; g++) {
                    userKeywords[g].id === users[i].keywords[j].id ? w[i].count += 1 : false;
                }
            }
            w[i].count === 0 ? delete w[i] : false
        }
    }

    w.sort(function (a, b) {
        if (a.count < b.count) {
            return 1;
        }
        if (a.count > b.count) {
            return -1;
        }
        return 0;
    });

    return w.slice(0, 3);
}

function findCourses(likeUsers, userCourses) {
    let recommend = {};
    for (let i = 0; i < likeUsers.length; i++) {
        for (let j = 0; j < userCourses.length; j++) {
            if (likeUsers[i].userId === userCourses[j].userId) {
                for (let g = 0; g < userCourses[j].courses.length; g++) {
                    let courseId = userCourses[j].courses[g].courseId;
                    recommend[courseId] !== undefined ? recommend[courseId] += 1 : recommend[courseId] = 1;
                }
            }
        }
    }

    let courses = [];
    for (key in recommend) {
        let course = {courseId: key, rating: recommend[key]};
        courses = [...courses, course];
    }

    courses.sort(function (a, b) {
        if (a.count < b.count) {
            return 1;
        }
        if (a.count > b.count) {
            return -1;
        }
        return 0;
    });

    return courses.slice(0, 3);
}

function coldStart(userId, users, userCourses) {
    //Find similar users
    let likeUsers = similarUsers(userId, users);

    //Find courses
    let recommendations = findCourses(likeUsers, userCourses);

    //Make an array
    let recommend = [];
    for (i = 0; i < recommendations.length; i++) {
        let course = {courseId: parseInt(recommendations[i].courseId), rating: -100};
        recommend = [...recommend, course];
    }

    return recommend.splice(0, 4);
}
module.exports = coldStart;