function popular(courses) {
    let recommend = [];
    courses.sort(function (a, b) {
        if (a.rating < b.rating) {
            return 1;
        }
        if (a.rating > b.rating) {
            return -1;
        }
        return 0;
    });
    
    let popularCourses = courses.splice(0, 4);
    popularCourses.forEach(function (course, i) {
        recommend[i] = {courseId: course.id, rating: -1};
    });

    return recommend;
}

module.exports = popular;