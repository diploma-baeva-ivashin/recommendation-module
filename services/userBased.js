function makeUserCoursesTable(userCourses) {
    let userCoursesTable = {};
    for (let i = 0; i < userCourses.length; i++) {
        let id = userCourses[i].userId;
        if (userCoursesTable[id]) {
            let courses = userCoursesTable[id].courses;
            userCoursesTable[id].courses = [...courses, userCourses[i]]
        } else {
            userCoursesTable[id] = {
                courses: [userCourses[i]],
                sum: 0,
                count: 0,
                id: userCourses[i].userId
            }
        }
        userCoursesTable[id].sum += userCourses[i].rating;
        (userCourses[i].rating !== null) ? userCoursesTable[id].count += 1 : false
    }
    return userCoursesTable;
}

function findCoeff(userId, users, userCoursesTable, thisUserCourses) {
    let w = [];

    for (let i = 0; i < users.length; i++) {
        let neighbor = users[i];
        if (neighbor.id !== userId && userCoursesTable[neighbor.id] !== undefined) {
            let sum1 = 0;
            let sum2 = 0;
            let sum3 = 0;
            for (let j = 0; j < userCoursesTable[neighbor.id].courses.length; j++) {
                for (let g = 0; g < thisUserCourses.courses.length; g++) {
                    let neighborCourse = userCoursesTable[neighbor.id].courses[j];
                    let neighborSum = userCoursesTable[neighbor.id].sum;
                    let neighborCount = userCoursesTable[neighbor.id].count;
                    let userCourse = thisUserCourses.courses[g];
                    if (neighborCourse.courseId === userCourse.courseId && neighborCourse.courseId !== null && userCourse.courseId !== null) {
                        sum1 += (userCourse.rating - thisUserCourses.sum / thisUserCourses.count) * (neighborCourse.rating - neighborSum / neighborCount);
                        sum2 += Math.pow((userCourse.rating - thisUserCourses.sum / thisUserCourses.count), 2);
                        sum3 += Math.pow((neighborCourse.rating - neighborSum / neighborCount), 2);
                    }
                }
            }
            w[i] = sum1 / (Math.sqrt(sum2) * Math.sqrt(sum3));
        }
    }
    return w;
}

function findRating(userId, users, userCoursesTable, thisUserCourses, w) {
    let rating = {};
    let sum1 = {};
    let sum2 = {};
    for (let i = 0; i < users.length; i++) {
        let neighbor = users[i];
        if (neighbor.id !== userId && userCoursesTable[neighbor.id] !== undefined) {
            for (let j = 0; j < userCoursesTable[neighbor.id].courses.length; j++) {
                let neighborCourse = userCoursesTable[neighbor.id].courses[j];
                let neighborAv = userCoursesTable[neighbor.id].sum / userCoursesTable[neighbor.id].count;
                if (neighborCourse.rating !== null) {
                    if (sum1[neighborCourse.courseId] !== undefined) {
                        sum1[neighborCourse.courseId] += (neighborCourse.rating - neighborAv) * w[i] * neighborCourse.endScore / 100;
                        sum2[neighborCourse.courseId] += Math.abs(w[i] * neighborCourse.endScore / 100);
                    } else {
                        sum1[neighborCourse.courseId] = 0;
                        sum2[neighborCourse.courseId] = 0;
                    }
                }
            }
        }
    }
    let thisUserCoursesAv = thisUserCourses.sum / thisUserCourses.count;
    let userAv = thisUserCourses.sum / thisUserCourses.count;
    for (let key in sum1) {
        rating[key] = thisUserCoursesAv + sum1[key] / sum2[key];
        rating[key] < userAv ? delete rating[key] : false
    }
    //Check if user pass this course yet
    for (let key in thisUserCourses.courses) {
        let course_id = thisUserCourses.courses[key].courseId;
        rating[course_id] ? delete rating[course_id] : false
    }
    return rating;
}

function userBased(userId, users, userCourses) {
    //Make userCourses table
    let userCoursesTable = makeUserCoursesTable(userCourses);

    //Find courses of our user
    let thisUserCourses = userCoursesTable[userId];

    //Find coefficient
    let w = findCoeff(userId, users, userCoursesTable, thisUserCourses);

    //Find rating
    let rating = findRating(userId, users, userCoursesTable, thisUserCourses, w);

    return rating;
}

module.exports = userBased;